import Confetti from "react-confetti";
import useWindowSize from "react-use/lib/useWindowSize";
import styles from "./Celebration.module.scss";

export interface ICelebrationProps {
  isBingo?: boolean;
}

const Celebration: React.FC<ICelebrationProps> = ({ isBingo }) => {
  const { width, height } = useWindowSize();

  const bingoCelebrations = [
    "https://i.giphy.com/ummeQH0c3jdm2o3Olp.webp",
    "https://i.giphy.com/cXblnKXr2BQOaYnTni.webp",
    "https://i.giphy.com/ynRrAHj5SWAu8RA002.webp",
  ];

  const randomIndex = Math.floor(Math.random() * bingoCelebrations.length);
  const randomCelebration = bingoCelebrations[randomIndex];

  return (
    <div className={styles.wrapper + " " + (isBingo ? styles.show : "")}>
      <img
        src={randomCelebration}
        alt="BINGO"
        className={
          styles.gif + " " + (isBingo ? styles.gifShow : styles.gifHide)
        }
      />
      {isBingo && (
        <Confetti
          width={width}
          height={height}
          numberOfPieces={400}
          gravity={0.3}
        />
      )}
    </div>
  );
};

export default Celebration;
