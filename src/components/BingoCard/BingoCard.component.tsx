import { useEffect, useState } from "react";
import { Card, Form } from "react-bootstrap";
import Skeleton from "react-loading-skeleton";
import animals from "../../mocks/animals.json";
import {
  IAnimal,
  animalsToRows,
  checkAllDirections,
  getRandomizedAnimals,
} from "../../utils/bingoArrayModifier";
import Celebration from "../Celebration/Celebration.component";
import styles from "./BingoCard.module.scss";
import BingoCardSkeleton from "./BingoCardSkeleton.component";

export interface IBingoCardProps {}

const BingoCard: React.FC<IBingoCardProps> = () => {
  const [bingoCard, setBingoCard] = useState<IAnimal[][]>([]);
  const [isBingo, setIsBingo] = useState(false);
  const [shouldKeepCard, setShouldKeepCard] = useState(false);
  const [isLoading, setIsLoading] = useState(true);

  // Load card from local storage or create a new one
  useEffect(() => {
    setIsLoading(true);
    const _keepCard = localStorage.getItem("keepCard");
    if (_keepCard === "true") {
      const _bingoCard = localStorage.getItem("bingoCard");

      setBingoCard(JSON.parse(_bingoCard || "[]"));
      setShouldKeepCard(JSON.parse(_keepCard || "false"));
      setIsLoading(false);
      return;
    }

    const randomizedAnimals = getRandomizedAnimals(animals);
    const _bingoCard = animalsToRows(randomizedAnimals);
    setBingoCard(_bingoCard);
    setIsLoading(false);
  }, []);

  // Check for bingo
  useEffect(() => {
    if (bingoCard.length > 0 && !isLoading) {
      const _isBingo = checkAllDirections(bingoCard);
      setIsBingo(_isBingo);

      if (_isBingo) {
        setTimeout(() => {
          setIsBingo(false);
        }, 4000);
      }
    }

    if (shouldKeepCard) {
      localStorage.setItem("bingoCard", JSON.stringify(bingoCard));
    }
  }, [bingoCard, isLoading]);

  // Toggle animal selection
  const toggleSelect = (rowIndex: number, picIndex: number) => {
    const updatedBingoCard = [...bingoCard];
    updatedBingoCard[rowIndex][picIndex].selected =
      !updatedBingoCard[rowIndex][picIndex].selected;
    setBingoCard(updatedBingoCard);
  };

  // I want to implement the logic of not shuffling the card if user checked the Kepp this card active
  const keepCard = (e: React.ChangeEvent<HTMLInputElement>) => {
    const _shouldKeepCard = e.target.checked;

    if (_shouldKeepCard) {
      localStorage.setItem("bingoCard", JSON.stringify(bingoCard));
    } else {
      localStorage.removeItem("bingoCard");
    }

    setShouldKeepCard(_shouldKeepCard);
    localStorage.setItem("keepCard", JSON.stringify(_shouldKeepCard));
  };

  return (
    <div>
      {isLoading && <BingoCardSkeleton />}
      {!isLoading && (
        <Card className={styles.BingoCard}>
          <Card.Header className={styles.BingoCardHeader}>
            Bingo Card
          </Card.Header>
          <Card.Body className={styles.BingoCardBody}>
            <Card.Text>
              {bingoCard.map((row, rowIndex) => (
                <div key={rowIndex} className="d-flex">
                  {row.map((pic, picIndex) => {
                    const isBingoColumn = pic.selected && pic.disabled;
                    return (
                      <div
                        key={pic.id}
                        className={
                          styles.BingoCardBodyColumn +
                          " " +
                          (pic.selected
                            ? styles.BingoCardBodyColumnSelected
                            : "") +
                          " " +
                          (isBingoColumn ? styles.BingoCardBodyColumnBingo : "")
                        }
                        onClick={() =>
                          !pic.disabled && toggleSelect(rowIndex, picIndex)
                        }
                      >
                        {isBingoColumn ? (
                          "🎲"
                        ) : (
                          <img
                            className={styles.BingoCardBodyColumnImage}
                            src={process.env.PUBLIC_URL + pic.value}
                            alt={pic.label}
                          />
                        )}
                      </div>
                    );
                  })}
                </div>
              ))}
            </Card.Text>
          </Card.Body>
        </Card>
      )}

      {isLoading && <Skeleton width={150} />}
      {!isLoading && (
        <Form.Check
          type="checkbox"
          id="keepValid"
          label="Save current card"
          className={styles.KeepCardCheckbox}
          checked={shouldKeepCard}
          onChange={keepCard}
        />
      )}

      <Celebration isBingo={isBingo} />
    </div>
  );
};

export default BingoCard;
