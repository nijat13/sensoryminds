import { Card } from "react-bootstrap";
import Skeleton from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";
import styles from "./BingoCard.module.scss";

export interface IBingoCardSkeletonProps {}

const BingoCardSkeleton: React.FC<IBingoCardSkeletonProps> = () => {
  return (
    <>
      <Card className={styles.BingoCard}>
        <Card.Header className={styles.BingCardHeader}>
          <Skeleton width={100} />
        </Card.Header>
        <Card.Body className={styles.BingoCardBody}>
          <Card.Text>
            {Array(5)
              .fill(null)
              .map((_, rowIndex) => (
                <div key={rowIndex} className="d-flex">
                  {Array(5)
                    .fill(null)
                    .map((_, columnIndex) => {
                      return (
                        <div
                          key={`${rowIndex}-${columnIndex}`}
                          className={styles.BingoCardBodyColumn}
                        >
                          <Skeleton circle={true} width={30} height={30} />
                        </div>
                      );
                    })}
                </div>
              ))}
          </Card.Text>
        </Card.Body>
      </Card>
    </>
  );
};

export default BingoCardSkeleton;
