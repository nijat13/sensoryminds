export interface IAnimal {
  id: number;
  label: string;
  value: string;
  key: string;
  selected?: boolean;
  disabled?: boolean;
}

export const animalsToRows = (animals: IAnimal[]) => {
  const cloneAnimals = animals.slice();

  const bingoObj: IAnimal = {
    id: 25,
    label: "BINGO",
    value: "BINGO",
    key: "BINGO",
    selected: true,
    disabled: true,
  };
  cloneAnimals.splice(2 * 5 + 2, 0, bingoObj);

  let newArray = [];
  for (let i = 0; i < 5; i++) {
    let row = [];
    for (let j = 0; j < 5; j++) {
      row.push(Object.assign({}, cloneAnimals[i * 5 + j]));
    }
    newArray.push(row);
  }

  return newArray;
};

export const getRandomizedAnimals = (animals: IAnimal[]) => {
  return animals.slice().sort(() => Math.random() - 0.5);
};

/**
 * [
 *   [0, 0, 0, 0, 0],
 *   [0, 0, 0, 0, 0],
 *   [0, 0, 1, 0, 0],
 *   [0, 0, 0, 0, 0],
 *   [0, 0, 0, 0, 0],
 * ]
 */

enum Direction {
  TOP_LEFT = "topLeft",
  TOP_RIGHT = "topRight",
}

interface SavedCombinations {
  diagonals: Direction[];
  rows: number[];
  columns: number[];
}

const savedCombinations: SavedCombinations = {
  diagonals: [],
  rows: [],
  columns: [],
};

export const checkAllDirections = (animalRows: IAnimal[][]) => {
  const checkColumnOrRowAlreadySaved = (
    dir: "rows" | "columns",
    index: number,
    isBingo: boolean = false
  ): boolean => {
    if (isBingo && !savedCombinations[dir].includes(index)) {
      savedCombinations[dir].push(index);
      return true;
    } else if (!isBingo && savedCombinations[dir].includes(index)) {
      let i = savedCombinations[dir].indexOf(index);

      if (i !== -1) {
        savedCombinations[dir].splice(i, 1);
      }
      return false;
    }

    return false;
  };

  // Check horizontally and vertically
  for (let i = 0; i < 5; i++) {
    let rowCount = 0;
    let colCount = 0;
    for (let j = 0; j < 5; j++) {
      if (animalRows[i][j].selected) {
        rowCount++;
      }
      if (animalRows[j][i].selected) {
        colCount++;
      }

      // Calculate the row and column count and reset if it's not a bingo
      if (j === 4) {
        rowCount = checkColumnOrRowAlreadySaved("rows", i, rowCount === 5)
          ? rowCount
          : 0;
        colCount = checkColumnOrRowAlreadySaved("columns", i, colCount === 5)
          ? colCount
          : 0;
      }
    }

    if (rowCount === 5 || colCount === 5) {
      return true;
    }
  }

  const checkDiagonals = (dir: Direction) => {
    let diagonalCount = 0;
    for (let i = 0; i < 5; i++) {
      const indexByDirection = dir === "topLeft" ? i : 5 - 1 - i;

      if (animalRows[i][indexByDirection].selected) {
        diagonalCount++;
      }
    }

    const isBingo = diagonalCount === 5;

    if (isBingo && !savedCombinations["diagonals"].includes(dir)) {
      savedCombinations["diagonals"].push(dir);
      return true;
    }
    if (!isBingo) {
      let index = savedCombinations["diagonals"].indexOf(dir);
      if (index !== -1) {
        savedCombinations["diagonals"].splice(index, 1);
      }
    }

    return false;
  };

  return (
    checkDiagonals(Direction.TOP_LEFT) || checkDiagonals(Direction.TOP_RIGHT)
  );
};
