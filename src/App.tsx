import { Container } from "react-bootstrap";
import "./App.scss";
import { BingoCard } from "./components";

function App() {
  return (
    <div className="App">
      <Container className="d-flex flex-column align-items-center">
        <BingoCard />
      </Container>
    </div>
  );
}

export default App;
